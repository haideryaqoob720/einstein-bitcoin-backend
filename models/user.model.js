module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
      fName: {
        type: Sequelize.STRING
      },
      sName: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      sAddress: {
        type: Sequelize.STRING
      },
      pCode: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.INTEGER, defaultValue: 0
      }
    });
  
    return User;
  };