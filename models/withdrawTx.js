module.exports = (sequelize, Sequelize) => {
    const transaction = sequelize.define('Withdrawtxs', {
        id: {
            type: Sequelize.INTEGER,
            unique: true,
            autoIncrement: true
        },
        user_id: {type: Sequelize.STRING, allowNull: false},
        uuid: { type: Sequelize.STRING(36), allowNull: false, primaryKey: true },
        type: { type: Sequelize.ENUM('deposit', 'withdraw'), allowNull: false, primaryKey: true },
        symbol: {type: Sequelize.STRING(10), allowNull: false},
        chain: { type: Sequelize.STRING(20), allowNull: false },
        currencyType: { type: Sequelize.STRING(20), allowNull: true, defaultValue: 'crypto' },
        amount: { type: Sequelize.STRING, allowNull: false, validate: { min: 0 } },
        withdrawn_amount: { type: Sequelize.STRING, allowNull: false, defaultValue: 0, validate: { min: 0 } },
        // confirmed_amount: { type: Sequelize.STRING, allowNull: false, defaultValue: 0, validate: { min: 0 } },
        receiver: { type: Sequelize.STRING(255), allowNull: false },
        sender: { type: Sequelize.STRING(42), allowNull: true },
        tx_id: { type: Sequelize.STRING(66), allowNull: true },
        status: { type: Sequelize.ENUM('success', 'pending', 'processing', 'error', 'rejected', 'failed', 'suspended'), allowNull: false },
        // error: { type: Sequelize.TEXT, allowNull: true },
        description: { type: Sequelize.STRING(255), allowNull: true },
        // executionMessage: { type: Sequelize.STRING(255), allowNull: true },
        txFee: { type: Sequelize.DECIMAL(30, 18), allowNull: true, defaultValue: 0 },
        network_fee: { type: Sequelize.STRING(255), defaultValue: 0, validate: { min: 0 } },
        contractFee: { type: Sequelize.STRING(255), defaultValue: 0, validate: { min: 0 } },
        createdAt: { type: Sequelize.INTEGER(15), allowNull: false, defaultValue: 0 },
        updatedAt: { type: Sequelize.INTEGER(15), allowNull: true, defaultValue: 0 }
    }, {
        timestamps: false
    })
  
    return transaction;
  };