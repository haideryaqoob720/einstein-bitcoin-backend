module.exports = (sequelize, Sequelize) => {
    const BTCaddress = sequelize.define("btcaddress", {
      btcAddress: {
        type: Sequelize.STRING
      },
      merchantId: {
        type: Sequelize.INTEGER
      },
      clientName: {
        type: Sequelize.STRING
      },
      apiKey: {
          type: Sequelize.STRING
      },
      clientInvoice: {
          type: Sequelize.INTEGER
      },
      isDeposit: {
        type: Sequelize.BOOLEAN
      },
      amount: {
        type: Sequelize.INTEGER, defaultValue: 0
      },
    });
  
    return BTCaddress;
  };