const jwt = require('jsonwebtoken');
require('dotenv').config();
const db = require("../models");
const User = db.user;

exports.update = (req, res) => {
    const {email} = req.body;

    User.findOne({email}, (err, user) => {
        if(err || !user){
            return res.status(400).json({error: "user with this email does not exixt"})
        }

        const token = jwt.sign({id: user.id, process.env.RESET_PASSWORD_KEY})
    })
}