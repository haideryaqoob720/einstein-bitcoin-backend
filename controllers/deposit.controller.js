kafka = require('../helpers/kafka');
const config = require('../../config/_local');
helper = require('../helpers/helper');
helper = require('../helpers/helper');
// const helper = require('../helpers/helper');
const db = require("../models");
const btc_address = db.address;
const Op = db.Sequelize.Op;
const btcCommandReply = 'wallet.command.reply';

// verify deposit
router.post("/", async function (req, res, next) {
    //ensure that you have a user with this email
    var email = await User.findOne({ where: { email: req.body.email } });
    if (email == null) {
        /**
         * we don't want to tell attackers that an
         * email doesn't exist, because that will let
         * them use this form to find ones that do
         * exist.
         **/
        return res.json({ status: 'ok' });
    }
    /**
     * Expire any tokens that were previously
     * set for this user. That prevents old tokens
     * from being used.
     **/
    await ResetToken.update({
        used: 1
    },
        {
            where: {
                email: req.body.email
            }
        });

    //Create a random reset token
    var token = crypto.randomBytes(64).toString('base64');

    //token expires after one hour
    var expireDate = new Date();
    expireDate.setDate(expireDate.getDate() + 1);

    //insert token data into DB
    await ResetToken.create({
        email: req.body.email,
        expiration: expireDate,
        token: token,
        used: 0
    });
    console.log("token: ", token);
    console.log("email: ", req.body.email);
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'haideryaqoobengr@gmail.com',
            pass: '012854136'
        }
    });

    var mailOptions = {
        from: 'haideryaqoobengr@gmail.com',
        to: req.body.email,
        subject: 'FORGET PASSWORD',
        text: 'To reset your password, please click the link below.\n\nhttp://' + 'localhost:8080' + '/Resetpass?token=' + encodeURIComponent(token) + '&email=' + req.body.email
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

    return res.json({ status: 'ok' });
});

// Retrieve all api keys from the database corresponding to merchant.
exports.findAll = (req, res) => {
    const merchantId = req.query.merchantId;
    // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    var condition = { merchantId: merchantId }

    MerchantApiKey.findAll({ where: condition })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving tutorials."
            });
        });
};

exports.update = (req, res) => {
    const id = req.params.id;
    generatedKey = generateUUID();
    req.body.key = generatedKey

    MerchantApiKey.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            res.send({
                message: "Shop name  and api key was updated successfully."
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Shop name  and api key with id=" + id
            });
        });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    MerchantApiKey.destroy({
        where: { id: id }
    })
        .then(num => {
            res.send({
                message: "API key was deleted successfully!"
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete API key with id=" + id
            });
        });
};