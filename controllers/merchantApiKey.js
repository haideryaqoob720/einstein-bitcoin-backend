const db = require("../models");
const MerchantApiKey = db.merchantApikey;

const Op = db.Sequelize.Op;

    // Create a api key function
    function generateUUID()
    {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
        {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });

    return uuid;
    }

// Create and Save a new Tutorial
exports.create = (req, res) => {
    // Validate request
    if (!req.body.shopName) {
      res.status(400).send({
        message: "Content can not be empty! There must be shop name"
      });
      return;
    }
    generatedKey = generateUUID();
    const shopKey = {
      shopName: req.body.shopName,
      key: generatedKey,
      merchantId: req.body.merchantId
    };
  
    // Save api key in the database
    MerchantApiKey.create(shopKey)
      .then(data => {
          console.log("New Api key created")
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the New API key."
        });
      });
  };

// Retrieve all api keys from the database corresponding to merchant.
exports.findAll = (req, res) => {
    const merchantId = req.query.merchantId;
    // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
    var condition = {merchantId: merchantId}
  
    MerchantApiKey.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
  };

  exports.update = (req, res) => {
    const id = req.params.id;
    generatedKey = generateUUID();
    req.body.key = generatedKey
  
    MerchantApiKey.update(req.body, {
      where: { id: id }
    })
      .then(num => {
          res.send({
            message: "Shop name  and api key was updated successfully."
          });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Shop name  and api key with id=" + id
        });
      });
  };

  exports.delete = (req, res) => {
    const id = req.params.id;
  
    MerchantApiKey.destroy({
      where: { id: id }
    })
      .then(num => {
          res.send({
            message: "API key was deleted successfully!"
          });
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete API key with id=" + id
        });
      });
  };