'use strict'

/*=========================
 * Kafka client works as both consumer and producer
 *=========================*/
// const path = require('path')
const Kafka = require('node-rdkafka')

const config = require('../../config/_local');
// const log = require('./logger').create("KAFKA");

const default_options = {
    // connection ops
    'client.id': "config.kafka.name",
    'metadata.broker.list': config.kafka.hosts,
    // 'debug': 'cgrp,topic,broker',

    // configurations
    'heartbeat.interval.ms': 5000,
    'socket.keepalive.enable': true,

    // "security.protocol": "ssl",
    // "ssl.key.password": "asdasd",
    // "ssl.ca.location": (path.join(__dirname, '../../ssl/ca-cert.pem')),
    // "ssl.certificate.location": (path.join(__dirname, '../../ssl/client.pem')),
    // "ssl.key.location": (path.join(__dirname, '../../ssl/client.key')),
}

// if ((config.util.getEnv('NODE_ENV') == '_local'))
//     delete default_options['security.protocol']

console.log("INITIALIZING KAFKA CONSUMER & PRODUCER")

/*========== SETTING UP DEFAULTS FOR KAFKA CONSUMER ==========*/

var consumer;
const options_consumer = {
    'fetch.wait.max.ms': 100,
    'enable.auto.commit': true,
    'group.id': `${config.kafka.name}_consumerasasas`
}
function connectConsumer() {
    return new Promise((resolve, reject) => {

        console.log(`Connecting to consumer @ hosts ${config.kafka.hosts}`)

        consumer = new Kafka.KafkaConsumer(Object.assign({}, default_options, options_consumer))

        consumer.connect({}, (err, res) => {
            if (err) {
                console.error(err)
                return reject(err)
            }
            console.log(`Consumer connection created: ${res.orig_broker_name}`)
            // console.log("consumer connected");
        })

        consumer.on("ready", function (arg) {
            console.log(config.kafka.consumer.topics)
            consumer.subscribe(['btc.wallet.command.reply', 'btc.wallet.transaction.confirmed']); //subscribing to all topics
            consumer.consume()
            resolve()

            console.log(`Consumer ${arg.name} subscribed to ${['btc.wallet.command.reply', 'btc.wallet.transaction.confirmed']}`);
        })

        consumer.on('data', function (data) {
            console.log("data in kafka file: ", data.value.toString());
        });

        consumer.on("error.error", function (error) {
            console.log(`Consumer ${arg.name} error ${error}`);
        });

        consumer.on('connection.failure', function (err) {
            console.log('[connection.failure] producer unable to connect ');
            reject(err.message)
        })

        consumer.on('disconnected', function (arg) {
            console.log(`Consumer disconnected: ${JSON.stringify(arg)}`);
        });

        setInterval(() => {
            console.log('Consumer connectedTime: ', consumer.connectedTime())
            const lastError = consumer.getLastError()
            if (lastError != null)
                console.log('Consumer lastError: ')
            // consumer.getMetadata({}, (err, res) => {
            //     console.log(res)
            // })
        }, 60000)
    })
}

/*//////////////////////////////////////////////////////////////*/


/*======== SETTING UP KAFKA PRODUCER FOR EVENT HUB =======*/

let producer;
let options_producer = Object.assign({}, default_options, {
    'dr_cb': true,
    'group.id': `${config.kafka.name}_producer`,
    'queue.buffering.max.ms': 0,

    // "debug": "generic,broker,security",
    // 'compression.codec': 'snappy'
})
let topicOpts = {
    'request.required.acks': 1,
    'produce.offset.report': true
};

function connectProducer() {
    return new Promise((resolve, reject) => {
        console.log(`Connecting to producer @ hosts ${config.kafka.hosts}`)
        producer = new Kafka.Producer(options_producer, topicOpts);
        producer.setPollInterval(config.kafka.producers.pollInterval); //polling for delivery reports
        producer.connect({}, (err, res) => {
            if (err) {
                console.error(err)
                return reject(err)
            }
            console.log(`Producer connection created: ${res.orig_broker_name}`)
        })
        producer.on('ready', function (arg) {
            console.log(`Producer ${arg.name} successfully connected`);
            resolve()
        })
        producer.on('connection.failure', function (err) {
            console.log('[connection.failure] producer unable to connect ');
            reject(err.message)
        })
        producer.on('close', function () {
            console.log('Producer successfully closed');
        })
        producer.on('event.error', function (err) {
            if (err.message != "all broker connections are down") {
                console.log('Error from producer: ', err);
            }
        })
        producer.on('delivery-report', function (err, report) {
            console.log('delivery-report: ' + JSON.stringify(report));
        });
    })
}

/*//////////////////////////////////////////////////////////////*/

/*==========                    HELPERS               ==========*/

// TODO: HANDLING ERROR
// Method to send messages to the given topic
// message is converted to JSON buffer before they are added in the queue
function send(topic, message) {
    try {
        // const msg = message;
        const msg = JSON.stringify(message)
        console.log("message in send function: ", msg)
        console.log(`[] Sending data to topic ${topic} message: ${msg}`);

        // convert objects to JSON strings
        message = Buffer.from(msg);

        //sending message
        producer.produce(topic, -1, message); // -1 for default partition
    } catch (error) {
        console.error(error);
    }
}

// add a listener to the consumer data
function consumer_on_data(cb) {
    consumer.on('data', cb)
}

function isConnected() {
    return producer.isConnected();
}

function closeConnections() {
    if (producer && producer.isConnected()) {
        producer.disconnect();
        console.log(`[PRODUCER] conenction closed`)
    }
    else
        console.log('[Producer] Conenction is already closed')

    if (consumer && consumer.isConnected()) {
        consumer.disconnect();
        console.log(`[CONSUMER] conenction closed`)
    }
    else
        console.log('[Producer] Conenction is already closed')
}

/*//////////////////////////////////////////////////////////////*/

// expose only these methods to the rest of the application and abstract away
module.exports.send = send;
module.exports.isConnected = isConnected;
module.exports.closeConnections = closeConnections;
module.exports.connectConsumer = connectConsumer;
module.exports.consumer_on_data = consumer_on_data;
module.exports.connectProducer = connectProducer;
