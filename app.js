const express = require('express');
const bodyParser = require("body-parser");
const cors = require("cors");
// const helper = require("./helpers/helper");

const app = express();


var corsOptions = {
  origin: "http://localhost:8080"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("../src/models");
const Role = db.role;


db.sequelize.sync({ force: true }).then(() => {
  console.log("Drop and re-sync db.");
  initial();
});

app.get("/", (req, res) => {
  res.json({ message: "Welcome to Einstein Bitcoin." });
});

const { authJwt } = require("../src/middleware");
// routes
// require("../src/routes/tutorial.routes")(app);
require('../src/routes/auth.routes')(app);
// require('../src/routes/user.routes')(app);
// require('../src/routes/commentOnTutorial.routes')(app);
// require('../src/routes/merchantApiKey.routes')(app);
// require('../src/routes/address.routes')(app);
require('../src/routes/forgetPassword.routes')(app);
// require('../src/routes/deposit.route')(app);
// require('../src/routes/merchant.routes')(app);
require('../src/routes/changePassword.routes')(app);

// app.post('/withdraw',[authJwt.verifyToken] , require('../src/routes/withdraw.route'))

// set port, listen for requests
const PORT = process.env.PORT || 8085;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

  function initial() {
    Role.create({
      id: 1,
      name: "user"
    });
   
    Role.create({
      id: 2,
      name: "merchant"
    });
   
    Role.create({
      id: 3,
      name: "admin"
    });
  }

//   process.on('SIGINT', async function () {
//     console.log('SIGINT Shutdown received.');
//     await helper.closeConnections();
//     process.exit(0)
// });

// // //Terminate active connection on kill
// process.on('SIGTERM', async function () {
//     console.log('SIGTERM Shutdown received.');
//     await helper.closeConnections();
//     process.exit(0)
// });