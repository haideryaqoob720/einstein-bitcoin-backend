// var crypto = require("crypto");
// var bcrypt = require("bcryptjs");
// var nodemailer = require('nodemailer');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const db = require("../models");
const Address = db.address;
// const User = db.user;

module.exports = app => {
    // const forgetPassword = require("../controllers/forgetPassword.controller");

    var router = require("express").Router();

    router.post("/", async function (req, res, next) {
        //ensure that you have a user with this email
        var depositRecod = await Address.findOne({ where: { btcAddress: req.body.address } });
        if (depositRecod == null) {
          return res.json({ message: 'address does not exist' });
      }
        // console.log("depositRecod: ", depositRecod);
        console.log("deposit amount: ", depositRecod.dataValues.amount);
        const depositAmount = depositRecod.dataValues.amount;
        if (depositAmount == 0){
            return res.json({ message: 'Deposit not done yet!' });
        }else if (depositAmount == req.body.amount || depositAmount > req.body.amount){
          return res.json({ message: 'Yes, Deposited' });
        }else if (depositAmount < req.body.amount){
          return res.json({ message: `Please deposit complete amount! Deposited Amount is: ${depositAmount}`})
        }
        // return res.json({ status: 'ok' });
    });

    app.use('/merchant/verifyDeposit', router);

};